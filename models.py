import torch
import torch.nn as nn
import torch.nn.functional as F


def conv_layer(in_channels, out_channels, kernel_size, stride=2, padding=1, batch_norm=True):
    """A helper function for creating convolutional layers

    Args:
        in_channels (int): The number of input channels to the layer
        out_channels (int): The number of output channels of the layer
        kernel_sizze (int): The size of the convolution kernel
        stride (int): The convolution stride
        padding (int): The padding added when performing the convolution
        batch_norm (bool): Whether or not a batch normalization step should be added

    Returns:
        A convolutional layer
    """
    layers = []
    conv_layer = nn.Conv2d(in_channels, out_channels, 
                           kernel_size, stride, padding, bias=False)

    # append conv layer
    layers.append(conv_layer)

    if batch_norm:
    # append batchnorm layer
        layers.append(nn.BatchNorm2d(out_channels))

    # using Sequential container
    return nn.Sequential(*layers)



def deconv(in_channels, out_channels, kernel_size, stride=2, padding=1, batch_norm=True):
    """A helper function for creating deconvolutional layers

    Args:
        in_channels (int): The number of input channels to the layer
        out_channels (int): The number of output channels of the layer
        kernel_sizze (int): The size of the convolution kernel
        stride (int): The convolution stride
        padding (int): The padding added when performing the convolution
        batch_norm (bool): Whether or not a batch normalization step should be added

    Returns:
        A deconvolutional layer
    """
    # create a sequence of transpose + optional batch norm layers
    layers = []
    transpose_conv_layer = nn.ConvTranspose2d(in_channels, out_channels, 
                                              kernel_size, stride, padding, bias=False)
    # append transpose convolutional layer
    layers.append(transpose_conv_layer)
    
    if batch_norm:
        # append batchnorm layer
        layers.append(nn.BatchNorm2d(out_channels))
        
    return nn.Sequential(*layers)



class Discriminator(nn.Module):
    """The discriminator network. A normal CNN classification network

    Args:
        conv_dim (int): The depth of the first convolutional layer
    """

    def __init__(self, conv_dim):
        super(Discriminator, self).__init__()
        
        self.conv_dim = conv_dim
        # 32x32 input
        self.conv1 = conv_layer(3, conv_dim, 4, batch_norm=False) # first layer, no batch_norm
        # 16x16
        self.conv2 = conv_layer(conv_dim, conv_dim*2, kernel_size=3, stride=1) # extra layer to increase depth
        # 16x16
        self.conv3 = conv_layer(conv_dim*2, conv_dim*2, 4)
        # 8x8
        self.conv4 = conv_layer(conv_dim*2, conv_dim*2, kernel_size=3, stride=1) # extra layer to increase depth
        # 8x8
        self.conv5 = conv_layer(conv_dim*2, conv_dim*4, 4)
        # 4x4
        # final, fully-connected layer
        self.fc = nn.Linear(conv_dim*4*4*4, 1)
     

    def forward(self, x):
        """The forward propagation through the neural network

        Args:
            x: The input to the neural network

        Returns:
            Discriminator logits; the output of the neural network
        """
        out = F.leaky_relu(self.conv1(x), 0.2)
        out = F.leaky_relu(self.conv2(out), 0.2)
        out = F.leaky_relu(self.conv3(out), 0.2)
        out = F.leaky_relu(self.conv4(out), 0.2)
        out = F.leaky_relu(self.conv5(out), 0.2)
        
        # flatten
        out = out.view(-1, self.conv_dim*4*4*4)
        
        # final output layer
        out = self.fc(out)        
        return out


class Generator(nn.Module):
    """The generator network.

    Args:
        z_size (int): The length of the input latent vector, z
        conv_dim (int): The depth of the inputs to the last transpose convolutional layer
    """
    
    def __init__(self, z_size, conv_dim):
        super(Generator, self).__init__()

        self.conv_dim = conv_dim
        
        # first, fully-connected layer
        self.fc = nn.Linear(z_size, conv_dim*4*4*4)

        # transpose conv layers
        # 4x4 input
        self.t_conv1 = deconv(conv_dim*4, conv_dim*2, 4)
        # 8x8
        self.t_conv2 = deconv(conv_dim*2, conv_dim*2, 3, stride=1) # extra layer to increase depth
        # 8x8
        self.t_conv3 = deconv(conv_dim*2, conv_dim*2, 3, stride=1) # extra layer to increase depth
        # 8x8
        self.t_conv4 = deconv(conv_dim*2, conv_dim, 4)
        # 16x16
        self.t_conv5 = deconv(conv_dim, conv_dim, 3, stride=1) # extra layer to increase depth
        # 16x16
        self.t_conv6 = deconv(conv_dim, 3, 4, batch_norm=False)
        # 32x32 output
        

    def forward(self, x):
        """The forward propagation through the neural network

        Args:
            x: The input to the neural network

        Returns:
            A 32x32x3 Tensor image
        """
        out = self.fc(x)
        out = out.view(-1, self.conv_dim*4, 4, 4) # (batch_size, depth, 4, 4)
        # hidden transpose conv layers + relu
        out = F.relu(self.t_conv1(out))
        out = F.relu(self.t_conv2(out))
        out = F.relu(self.t_conv3(out))
        out = F.relu(self.t_conv4(out))
        out = F.relu(self.t_conv5(out))

        # last layer + tanh activation
        out = self.t_conv6(out)
        out = torch.tanh(out)
        return out


def weights_init_normal(m):
    """Applies initial weights to certain layers in a model .
    The weights are taken from a normal distribution 
    with mean = 0, std dev = 0.02.

    Args:
        m: A module or layer in a network
    """
    classname = m.__class__.__name__
    # Apply initial weights to convolutional and linear layers
    if classname in ["Conv2d", "Linear"]:
        m.weight.data.normal_(0., 0.02)


def build_network(d_conv_dim, g_conv_dim, z_size):
    """Build both networks

    Args:
        d_conv_dim (int): The convolution dimension of the discriminator network
        g_conv_dim (int): The (de)convolution dimension of the generator network
        z_size (int): The size of latent space vector z

    Returns:
        D: The discriminator network
        G: The generator network
    """
    
    # define discriminator and generator
    D = Discriminator(d_conv_dim)
    G = Generator(z_size=z_size, conv_dim=g_conv_dim)

    # initialize model weights
    D.apply(weights_init_normal)
    G.apply(weights_init_normal)
    
    return D, G


def load_generator(fp="generator.pt", z_size=100, conv_dim=32):
    G = Generator(z_size=z_size, conv_dim=conv_dim)
    G.load_state_dict(torch.load(fp, map_location=torch.device('cpu')))
    G.eval()
    return G
