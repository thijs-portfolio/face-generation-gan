import tkinter as tk
from PIL import ImageTk, ImageDraw
import utils
import models


def start(G, z_size=100):

    def update(id_, val):
        # print("id: {}, val: {}".format(id_, val))
        z[0][id_] = float(val)
        img = utils.generate(G, z, show=False)
        img = img.resize((150, 150))
        tkpi = ImageTk.PhotoImage(image=img)
        label_image.configure(image=tkpi)
        label_image.image = tkpi


    def button_click_exit_mainloop (event):
        event.widget.quit() # this will cause mainloop to unblock.


    root = tk.Tk()
    # root.bind("<Button>", button_click_exit_mainloop)
    root.geometry('300x300')

    canvas = tk.Canvas(root)
    scroll_y = tk.Scrollbar(canvas, orient="vertical", command=canvas.yview)
    frame = tk.Frame(canvas)


    #load the image file we created before
    z = utils.random_z(z_size)
    img = utils.generate(G, z_size=z_size, show=False)
    img = img.resize((150, 150))
    tkpi = ImageTk.PhotoImage(image=img)
    label_image = tk.Label(root, image=tkpi)
    label_image.pack()



    for i in range(z_size):
        w = tk.Scale(frame, from_=-1, to=1, 
                          resolution=0.1, 
                          length=180,
                          orient=tk.HORIZONTAL, 
                          showvalue=0,
                          command=lambda x, idx=i: update(idx, x))
        w.set(float(z[0][i]))
        w.pack()

    canvas.create_window(150, 150, anchor='center', window=frame)
    canvas.update_idletasks()
    canvas.configure(scrollregion=(0, -8*z_size, 0, 12*z_size), 
                     yscrollcommand=scroll_y.set) 
    canvas.pack(fill='both', expand=True, side="left")
    scroll_y.pack(fill='y', side='right')
  
    root.mainloop()