from datetime import datetime
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets
from torchvision import transforms
from PIL import Image
from matplotlib import pyplot as plt
import models

class Logger(object):
    """A logger object to print out messages and directly log them into a file.

    Args:
        log_fp (str): The file path where the log messages should be writtent to
        fn_stamp (bool): Whether or not the file name should be given a unique
            time stamp.
    """

    def __init__(self, log_fp, fn_stamp=True):
        ts = datetime.now()
        date = ts.strftime("_%d_%m_%y")
        self.log_fp = log_fp[:-4] + date + ".txt" if fn_stamp else log_fp

        init_msg = "Training log file. Created on {}"
        init_msg = init_msg.format(ts.strftime("%d %b %Y, %H:%M:%S"))
        with open(self.log_fp, "w") as f:
            f.write("\n".join(["-"*10, init_msg, "-"*10, "", ""]))

    def add(self, itm, stamp=True):
        """Add a log message to the file and print it

        Args:
            itm (str): The log message to add.
            stamp (bool): Whether the log message should be prepended with a
                timestamp
        """
        print(itm)
        msg = []
        if stamp:
            msg.append("|---" + datetime.now().strftime("%H:%M:%S") + "---")
        msg += [itm, "-"*15, "", ""]
        with open(self.log_fp, "a") as f:
            f.write("\n".join(msg))



def get_dataloader(batch_size=128, image_size=32, data_dir="training_data"):
    """Define a dataloader

    Args:
        batch_size (int): The batch size of the traning dataloader
        image_size (int): The size that each image in the dataset should have
        data_dir (str): The path to the folder containing the images.

    Returns:
        The dataloader
    """
    transf = transforms.Compose([transforms.Resize(image_size),
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                ])
    face_dataset = datasets.ImageFolder(data_dir, transform=transf)
    face_loader = torch.utils.data.DataLoader(face_dataset, batch_size=batch_size, shuffle=True),
    return face_loader


def real_loss(D_out):
    """Calculates how close discriminator outputs are to being real.
    
    Args:
        D_out: Discriminator logits

    Returns:
        The binary cross entropy loss of real network output logits
    """
    batch_size = D_out.size(0)
    # label smoothing
    labels = torch.ones(batch_size)*0.9
    
    # GPU is assumed to be availabe
    labels = labels.cuda()
    criterion = nn.BCEWithLogitsLoss()
    loss = criterion(D_out.squeeze(), labels)
    return loss

def fake_loss(D_out):
    """Calculates how close discriminator outputs are to being fake.

    Args:
        D_out: Discriminator logits

    Returns:
        The binary cross entropy loss of fake network output logits

    """
    batch_size = D_out.size(0)
    labels = torch.zeros(batch_size)

    # GPU is assumed to be available    
    labels = labels.cuda()
    criterion = nn.BCEWithLogitsLoss()
    loss = criterion(D_out.squeeze(), labels)
    return loss


def get_optimizers(D, G, lr=0.0002, beta1=0.5, beta2=0.999):
    """Create optimizers for the discriminator D and generator G

    Args:
        D: The discriminator network
        G: The generator network
        lr (float): The learning rate
        beta1 (float): Adam optimizer first momentum term
        beta2 (float): Adam optimizer second momentum term

    Returns:
        The optimizer for the discriminator network
        The optimizer for the generator network
    """
    # For now I tested only with the Adam optimizer
    d_optimizer = optim.Adam(D.parameters(), lr, [beta1, beta2])
    g_optimizer = optim.Adam(G.parameters(), lr, [beta1, beta2])
    return d_optimizer, g_optimizer


def random_z(z_size=100):
    """Generate a random latent space vector z with a random uniform distribution

    Args:
        z_size: The length of the latent space vector z (defaults to 100)

    Returns:
        torch tensor: A tensor of length 'z_size', populated with uniformly 
            distributed random values 
    """
    z = np.random.uniform(-1, 1, size=(1, z_size))
    z = torch.from_numpy(z).float()
    return z


def generate(G, z=None, z_size=100, show=False):
    """Generate a face

    Args:
        G: The generator model
        z: The latent space vector z. Using None results in a randomly generated one

    Returns:
        Image: The generated PIL image
    """
    # If Z is not specified make a random one
    if z is None:
        z = random_z(z_size)
    elif isinstance(z, np.ndarray):
        z = torch.from_numpy(z).float()

    if torch.cuda.is_available():
        G.cuda()
        z.cuda()
    else:
        G.cpu()
        z.cpu()

    im = G(z).cpu().detach().numpy()
    im = np.transpose(np.squeeze(im), (1, 2, 0))
    im = (im + 1)*255 / 2
    im = Image.fromarray(im.astype(np.uint8))

    if show:
        plt.imshow(im)
        plt.show()

    return im

