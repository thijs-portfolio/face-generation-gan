"""This script is used to extract the sample images generated during training"""

import os
import pickle
import shutil
import argparse
import numpy as np
from PIL import Image

parser = argparse.ArgumentParser()

parser.add_argument("-f",
                    "--file",
                    default="training_samples.pkl",
                    help="The file path where the trained model are saved")

parser.add_argument("-o",
                    "--output",
                    default="train_samples",
                    help="The output path where the images should be extracted")



if __name__ == "__main__":

    args = parser.parse_args()

    if os.path.isdir(args.output):
        shutil.rmtree(args.output)

    with open(args.file, "rb") as f:
        samples = pickle.load(f)

    os.mkdir(args.output)

    for i, sample in enumerate(samples):
        os.mkdir(os.path.join(args.output, "epoch_"+str(i)))
        for img_nr in range(sample.shape[0]):
            img = np.transpose(np.squeeze(sample[img_nr, :, :]), (1, 2, 0))
            img = (img + 1)*255 / 2
            im = Image.fromarray(img.astype(np.uint8))
            im.save(os.path.join(args.output, "epoch_{}", "sample_{}.png").format(i, img_nr))
    print("Done")