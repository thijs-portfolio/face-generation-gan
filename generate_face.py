"""The main program"""

import os
import argparse
import subprocess
import time
import torch

import models
import utils
import training
import ui

parser = argparse.ArgumentParser()


parser.add_argument("-s", 
                    "--save",
                    action="store_true",
                    help="Directly save the generated output")

parser.add_argument("--no_ui",
                    action="store_true",
                    help=("Do not start a UI session and instead generate a random image "
                          "and save it as a file"))

parser.add_argument("-sp", 
                    "--save_path",
                    default="face.png",
                    help="The model output image path to save")

parser.add_argument("-m", 
                    "--model",
                    default="generator.pt",
                    help="The pre-trained generator model to load")

parser.add_argument("-d", 
                    "--data_directory",
                    default="training_data",
                    help="A directory containing a collection of images for training.")


parser.add_argument("-t",
                    "--train",
                    action="store_true",
                    help="Train the model. Run the script in train mode")


parser.add_argument("-e",
                    "--n_epochs",
                    type=int,
                    default=10,
                    nargs="?",
                    help="Training hyperparameter: The number of epochs to train the model")

parser.add_argument("-b",
                    "--batch_size",
                    type=int,
                    default=128,
                    nargs="?",
                    help="Training hyperparameter: The batch_size used in training")


parser.add_argument("-l",
                    "--learning_rate",
                    type=float,
                    default=0.002,
                    nargs="?",
                    help="Training hyperparameter: The learning rate to be used in training")

parser.add_argument("--d_conv_dim",
                    type=int,
                    default=32,
                    nargs="?",
                    help="Model hyperparameter: The convolution dimension of the Discriminator network")

parser.add_argument("--g_conv_dim",
                    type=int,
                    default=32,
                    nargs="?",
                    help="Model hyperparameter: The convolution dimension of the Generator network")

parser.add_argument("-z",
                    "--z_size",
                    type=int,
                    default=100,
                    nargs="?",
                    help="Model hyperparameter: The length of the input latent vector, z")

parser.add_argument("-o",
                    "--train_output",
                    default="generator.pt",
                    help="The file path where the trained model should be saved")

parser.add_argument("-ts",
                    "--train_samples",
                    default="training_samples.pkl",
                    help=("A periodic save of model samples used to inspect the performance "
                          "during training."))

parser.add_argument("-aws", "--aws",
                    action="store_true",
                    help=("The model is trained on an AWS instance. Adding this flag makes sure that "
                          "the AWS instance is automatically closed when training has finished."))

parser.add_argument("--aws_bucket",
                    default="facegenerator",
                    help=("The AWS bucket name where the trained model should be saved, "
                          "(in case training is done on an AWS instance)"))




def aws_close(log):
    """For AWS training sessions, the output model, log and sample files should
    be copied to the S3 storage bucket, and the instance should be shut down when
    training is completed.

    Args:
        log: The logger object
    """
    if os.path.isfile(args.train_output):
        cmd = ["aws", "s3", "cp", args.train_output, "s3://{}".format(args.aws_bucket)]
        subprocess.call(cmd)
    if os.path.isfile(args.train_samples):
        cmd = ["aws", "s3", "cp", args.train_samples, "s3://{}".format(args.aws_bucket)]
        subprocess.call(cmd)
    if os.path.isfile(log.log_fp):
        cmd = ["aws", "s3", "cp", log.log_fp, "s3://{}".format(args.aws_bucket)]
        subprocess.call(cmd)
    if os.path.isfile("losses.pkl"):
        cmd = ["aws", "s3", "cp", "losses.pkl", "s3://{}".format(args.aws_bucket)]
        subprocess.call(cmd)
    print("Terminating in:")
    time.sleep(1)
    print("3")
    time.sleep(1)
    print("2")
    time.sleep(1)
    print("1")
    time.sleep(1)
    subprocess.call(["sudo", "poweroff"])


def generate(G, z=None, z_size=100, show=False):
    """Generate a face

    Args:
        G: The generator model
        z: The latent space vector z. Using None results in a randomly generated one

    Returns:
        Image: The generated PIL image
    """
    # If Z is not specified make a random one
    if z is None:
        z = np.random.uniform(-1, 1, size=(1, z_size))
        z = torch.from_numpy(z).float()

    if torch.cuda.is_available():
        G.cuda()
        z.cuda()
    else:
        G.cpu()
        z.cpu()

    im = G(z).cpu().detach().numpy()
    im = np.transpose(np.squeeze(im), (1, 2, 0))
    im = (im + 1)*255 / 2
    im = Image.fromarray(im.astype(np.uint8))
    return im


if __name__ == "__main__":
    args = parser.parse_args()

    if args.train:

        log = utils.Logger("train_log.txt")
        # Check for a GPU
        if not torch.cuda.is_available():
            errmsg = "No GPU found. Could not train the network."
            log.add(errmsg)
            if args.aws:
                aws_close()
            exit()
        print('Training on GPU!')
        # Set up the dataloaders
        data_loader = utils.get_dataloader(data_dir=args.data_directory)
        # Create the discriminator and generator models
        D, G = models.build_network(args.d_conv_dim, args.g_conv_dim, args.z_size)
        # Get the optimizers
        d_optim, g_optim = utils.get_optimizers(D, G, 
                                                lr=args.learning_rate,
                                                beta1=0.5, 
                                                beta2=0.999)
        D, G, losses = training.train(D, G, data_loader, d_optim, g_optim, 
                                      args.n_epochs, 
                                      args.z_size,
                                      generator_path=args.train_output, 
                                      samples_path=args.train_samples,
                                      log=log)
        log.add("Training has finished")



        if args.aws:
            aws_close(log)

    else:
        G = models.load_generator(args.model, args.z_size, args.g_conv_dim)

        if args.no_ui:
            im = utils.generate(G, z_size=args.z_size, show=not args.save)
            if args.save:
                im.save(args.save_path)
        else:
            ui.start(G, z_size=args.z_size)

