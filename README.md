# Face generation: Deep Convolutional Generative Adversarial Netork

This is my implementation of a DCGAN trained on the CelebA celebrity face dataset, made available by the University of Hongkong. This framework can be used to train the model on this dataset (or any image dataset). The training can be done locally if CUDA is avaiable, training and automatic shutdown is also possible.

![Modifying the latent space variables](https://gitlab.com/thijs-portfolio/face-generation-gan/-/raw/master/facegen.gif)

I created a very basic UI which allows for playing around with the latent space vector and observe its effects on the generated image.

To generate custom faces you can run the main script without any options. This will open a UI to modify the model output.

    $ python3 generate_face.py

If a UI is not desired, the '--no_ui' flag may be added, in which case a random image will be generated and saved to the path specified with the '-s' arg.

In order to train the DCGAN on a different dataset use the following

    $ python3 generate_face.py --train -d [data directory] -o [model output path]

When trained on an AWS EC2 instance, the '--aws' and '--aws_bucket' arguments can be added to automatically upload the results and shut down the instance when training has finished. (See -h for more info).

The following hyperparameters may be given as arguments:

    --n_epochs
    --batch_size
    --learning_rate
    --d_conv_dim
    --g_conv_dim
    --z_size

See --help for more parameters and information.


## Udacity

This project premise and dataset was provided by [Udacity](https://www.udacity.com/course/deep-learning-nanodegree--nd101) as part of their 4 month deep learning nanodegree course. This code was my graded project submission. I since rewrote everything in order to create a stand-alone command line app and UI interface.

.