import os
import pickle
import numpy as np
import torch
import utils


def train(D, G, dataloader, d_optimizer, g_optimizer, n_epochs=50, 
          z_size=100, print_every=50, generator_path=None, 
          samples_path=None, log=None):
    """Trains adversarial networks for some number of epochs.

    Args: 
        D: The discriminator network
        G: The generator network
        dataloader: The dataloader providing the face images
        d_optimizer: The optimizer for the discriminator network
        g_optimizer: The optimizer for the generator network
        n_epochs (int): The number of epochs to train for
        z_size (int): The size of the latent space vector z
        print_every (int): Interval in when intermediate progress should be
            printed and logged
        generator_path (str): The path where the intermediate generator model
            should be saved
        samples_path (str): The path where the training samples should be saved.
            These samples can be used to inspect the model training progress
        log: The logger object used to log and print training progress

    Returns:
        D: The trained discriminator network
        G: The trained generator network
        losses (ist): The generator and discrimenator losses throughout the training
    """
    
    if log is not None:
        log.add("Starting training...")

    # move models to GPU
    D.cuda()
    G.cuda()

    # keep track of loss and generated, "fake" samples
    losses = []
    samples = []

    # Get some fixed data for sampling. These are images that are held
    # constant throughout training, and allow us to inspect the model's performance
    sample_size=16
    fixed_z = np.random.uniform(-1, 1, size=(sample_size, z_size))
    fixed_z = torch.from_numpy(fixed_z).float()
    fixed_z = fixed_z.cuda()

    # epoch training loop
    for epoch in range(n_epochs):

        # batch training loop
        for batch_i, (real_images, _) in enumerate(dataloader[0]):

            batch_size = real_images.size(0)

            d_optimizer.zero_grad()
            g_optimizer.zero_grad()

            # 1. Train the discriminator on real and fake images
            real_images = real_images.cuda()
        
            D_real = D(real_images)
            d_real_loss = utils.real_loss(D_real)
            
            # Train on fake images
            z = np.random.uniform(-1, 1, size=(batch_size, z_size))
            z = torch.from_numpy(z).float()
            z = z.cuda()

            fake_images = G(z)
            
            D_fake = D(fake_images)
            d_fake_loss = utils.fake_loss(D_fake)
            
            d_loss = d_real_loss + d_fake_loss
            d_loss.backward()
            d_optimizer.step()
            
            # 2. Train the generator with an adversarial loss
            z = np.random.uniform(-1, 1, size=(batch_size, z_size))
            z = torch.from_numpy(z).float()
            z = z.cuda()
            fake_images = G(z)
            
            D_fake = D(fake_images)
            g_loss = utils.real_loss(D_fake)
            
            g_loss.backward()
            g_optimizer.step()
            
            # Print some loss stats
            if batch_i % print_every == 0:
                # append discriminator loss and generator loss
                losses.append((d_loss.item(), g_loss.item()))
                # print discriminator and generator loss
                msg = ('Epoch [{:5d}/{:5d}] | d_loss: {:6.4f} | g_loss: {:6.4f}'.format(
                       epoch+1, n_epochs, d_loss.item(), g_loss.item()))
                if log is not None:
                    log.add(msg)

        G.eval() # for generating samples
        samples_z = G(fixed_z)
        samples_np = samples_z.cpu().detach().numpy()
        samples.append(samples_np)

        # Saving intermediary generator model
        if generator_path is not None:
            msg = "Saving the intermediate generator model"
            if log is not None:
                log.add(msg)
            torch.save(G.state_dict(), generator_path)

        if samples_path is not None:
            with open(samples_path, "wb") as f:
                pickle.dump(samples, f, -1)

        G.train() # back to training mode

    # Save the losses
    if samples_path is not None:
        with open(os.path.abspath(os.path.join(samples_path, "..", 
                                               "losses.pkl")), "wb") as f:
            pickle.dump(losses, f, -1)

    return D, G, losses